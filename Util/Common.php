<?php
/**
 * Basic util functions.
 *
 * @author    Greg Sherwood <gsherwood@squiz.net>
 * @copyright 2006-2015 Squiz Pty Ltd (ABN 77 084 670 600)
 * @license   https://github.com/squizlabs/PHP_CodeSniffer/blob/master/licence.txt BSD Licence
 */

namespace PHP_CodeSniffer\Standards\Dsn12\Util;

class Common
{

    /**
     * An array of variable types for param/var we will check.
     *
     * @var string[]
     */
    public static $allowedTypes = [
        'array',
        'boolean',
        'float',
        'integer',
        'mixed',
        'object',
        'string',
        'resource',
        'callable',
    ];



    /**
     * Returns true if the specified string is in the snake stile format.
     *
     * @param string  $string      The string the verify.
     * @param boolean $classFormat If true, check to see if the string is in the
     *                             class format. Class format strings must start
     *                             with a capital letter and contain no
     *                             underscores.
     * @param boolean $public      If true, the first character in the string
     *                             must be an a-z character. If false, the
     *                             character must be an underscore. This
     *                             argument is only applicable if $classFormat
     *                             is false.
     * @param boolean $strict      If true, the string must not have two capital
     *                             letters next to each other. If false, a
     *                             relaxed camel caps policy is used to allow
     *                             for acronyms.
     *
     * @return boolean
     */
    //public static function isCamelCaps(
    public static function isSnakeStyle(
        $string,
        $classFormat=false,
        $public=true,
        $strict=true
    ) {
        // Check the first character first.
        if ($classFormat === false) {
            $legalFirstChar = '';
            if ($public === false) {
                #>$legalFirstChar = '[_]';
                $legalFirstChar = '[a-z]';
            }

            if ($strict === false) {
                // Can either start with a lowercase letter, or multiple uppercase
                // in a row, representing an acronym.
                #>$legalFirstChar .= '([A-Z]{2,}|[a-z])';
                $legalFirstChar .= '[a-z]';
            } else {
                $legalFirstChar .= '[a-z]';
            }
        } else {
            //$legalFirstChar = '[A-Z]';
            $legalFirstChar = '[a-z]';
        }

        if (preg_match("/^$legalFirstChar/", $string) === 0) {
            return false;
        }

        // Check that the name only contains legal characters.
        #>$legalChars = 'a-zA-Z0-9';
        $legalChars = 'a-z0-9_';
        if (preg_match("|[^$legalChars]|", substr($string, 1)) > 0) {
            return false;
        }
/*
        if ($strict === true) {
            // Check that there are not two capital letters next to each other.
            $length          = strlen($string);
            $lastCharWasCaps = $classFormat;

            for ($i = 1; $i < $length; $i++) {
                $ascii = ord($string[$i]);
                if ($ascii >= 48 && $ascii <= 57) {
                    // The character is a number, so it cant be a capital.
                    $isCaps = false;
                } else {
                    if (strtoupper($string[$i]) === $string[$i]) {
                        $isCaps = true;
                    } else {
                        $isCaps = false;
                    }
                }

                if ($isCaps === true && $lastCharWasCaps === true) {
                    return false;
                }

                $lastCharWasCaps = $isCaps;
            }
        }//end if
*/
        return true;

    }//end isCamelCaps()


    

}//end class
