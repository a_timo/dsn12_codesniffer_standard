<?php
/**
 * \Drupal\Sniffs\WhiteSpace\EmptyLinesSniff.
 *
 * @category PHP
 * @package  PHP_CodeSniffer
 * @link     http://pear.php.net/package/PHP_CodeSniffer
 */

namespace PHP_CodeSniffer\Standards\Dsn\Sniffs\WhiteSpace;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;

/**
 * \Drupal\Sniffs\WhiteSpace\EmptyLinesSniff.
 *
 * Checks that there are not more than x empty lines following each other.
 *
 * @category PHP
 * @package  PHP_CodeSniffer
 * @link     http://pear.php.net/package/PHP_CodeSniffer
 */
class IfEmptyLinesSniff implements Sniff
{
    protected $insteadBefore = [
        'T_OPEN_CURLY_BRACKET',
        'T_DOC_COMMENT',
        'T_COMMENT',
        //todo do we need comment string ?
    ];

    protected $insteadAfter = [
        'T_CLOSE_CURLY_BRACKET',
        'T_ELSE',
        'T_ELSEIF',
        'T_DOC_COMMENT',
        'T_COMMENT',
    ];

    /**
     * A list of tokenizers this sniff supports.
     *
     * @var array<string>
     */
    public $supportedTokenizers = [
        'PHP'
    ];

    /**
     * Registers the tokens that this sniff wants to listen for.
     *
     * @return int[]
     */
    public function register() {
        return [T_IF, T_ELSE, T_ELSEIF];
    }//end register()


    /**
     * Processes this test, when one of its tokens is encountered.
     *
     * @param \PHP_CodeSniffer\Files\File $phpcsFile The file being scanned.
     * @param int                         $stackPtr  The position of the current token in the
     *                                               stack passed in $tokens.
     *
     * @return void
     */
    public function process(File $phpcsFile, $stackPtr) {
        $tokens = $phpcsFile->getTokens();
        // before and after IF
        if ($tokens[$stackPtr]['code'] === T_IF) {            
            $prevTokens = $this->getBeforeIfTokens($stackPtr, $tokens);
            
            if (!$this->isEmptyLine($prevTokens, $this->insteadBefore)) {
                $error = 'Before IF empty line required';
                $phpcsFile->addError($error, ($stackPtr), 'IfEmptyLines');
            }

            // сыпятся ошибки, если if без {}
            if (!isset($tokens[$stackPtr]['scope_closer'])) {
                return;
            }

            $closeBracket = $tokens[$stackPtr]['scope_closer'];
            $afterTockens = $this->getAfterIfTokens($closeBracket, $tokens);

            if (!$this->isEmptyLine($afterTockens, $this->insteadAfter)) {
                $error = 'After IF empty line required';
                $phpcsFile->addError($error, ($closeBracket), 'IfEmptyLines');
            }            
        }
        
        if ($tokens[$stackPtr]['code'] === T_ELSE) {

            // сыпятся ошибки, если if без {}
            if (!isset($tokens[$stackPtr]['scope_closer'])) {
                return;
            }

            $closeBracket = $tokens[$stackPtr]['scope_closer'];
            $afterTockens = $this->getAfterIfTokens($closeBracket, $tokens);

            if (!$this->isEmptyLine($afterTockens, $this->insteadAfter)) {
                $error = 'After ELSE empty line required';
                $phpcsFile->addError($error, ($closeBracket), 'IfEmptyLines');
            } 
        }

        if ($tokens[$stackPtr]['code'] === T_ELSEIF) {

            // сыпятся ошибки, если if без {}
            if (!isset($tokens[$stackPtr]['scope_closer'])) {
                return;
            }

            $closeBracket = $tokens[$stackPtr]['scope_closer'];
            $afterTockens = $this->getAfterIfTokens($closeBracket, $tokens);

            if (!$this->isEmptyLine($afterTockens, $this->insteadAfter)) {
                $error = 'After ELSEIF empty line required';
                $phpcsFile->addError($error, ($closeBracket), 'IfEmptyLines');
            } 
        }


    }//end process()


    /**
     * Get tockens from previos line
     */ 
    protected function getBeforeIfTokens($idx, &$tokens) {
        $result = [];
        
        $line = $tokens[$idx]['line'] - 1;

        if ($line < 1) {
            return $result;
        }

        for ($i = $idx; $i > 0; $i--) {
            if ($tokens[$i]['line'] < $line) { // second line after if...
                break;
            }

            if ($tokens[$i]['line'] == $line) {
                $result[$i] = $tokens[$i];
            }
        }

        return $result;
    }


    /**
     * All tokens on line is T_WHITESPACE
     */ 
    protected function isEmptyLine($tokens_slice, $instead = []) {
        
        if (empty($tokens_slice)) {
            return true;
        }

        foreach ($tokens_slice as $k => $v) {
            if (in_array($v['type'], $instead)) {
                return true;
            }
        }

        foreach ($tokens_slice as $k => $v) {
            if ($v['type'] !== 'T_WHITESPACE') {
                return false;
            }
        }

        return true;
    }

    protected function getAfterIfTokens($idx, &$tokens) {
        $result = [];
        //var_export($idx);
        $line = $tokens[$idx]['line'] + 1;

        /*if (isset($line = $tokens[$idx + 1])) {
            return $result;
        }*/

        $i = $idx+1;
        $n = $i + 100;
        for ($i; $i < $n; $i++) {

            if (empty($tokens[$i]) || $tokens[$i]['line'] > $line) { // second line after if...
                break;
            }


            $result[$i] = $tokens[$i];
        }
        return $result;
    }


}//end class
