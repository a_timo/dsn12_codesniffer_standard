## About
Standarts for PHP_CodeSniffer (phpcs)


## The Dsn12 sniffs
`phpcs --standard=/path/to/standard/directory/Dsn12 -e`

```
The DSN12 standard contains 45 sniffs

Dsn12 (2 sniffs)
----------------
  Dsn12.Arrays.Array
  Dsn12.NamingConventions.ValidVariableName

Generic (15 sniffs)
-------------------
  Generic.ControlStructures.InlineControlStructure
  Generic.Files.ByteOrderMark
  Generic.Files.LineEndings
  Generic.Files.LineLength
  Generic.Formatting.DisallowMultipleStatements
  Generic.Functions.FunctionCallArgumentSpacing
  Generic.NamingConventions.UpperCaseConstantName
  Generic.PHP.DisallowAlternativePHPTags
  Generic.PHP.DisallowShortOpenTag
  Generic.PHP.LowerCaseConstant
  Generic.PHP.LowerCaseKeyword
  Generic.PHP.LowerCaseType
  Generic.WhiteSpace.DisallowTabIndent
  Generic.WhiteSpace.IncrementDecrementSpacing
  Generic.WhiteSpace.ScopeIndent

PEAR (1 sniff)
---------------
  PEAR.Functions.ValidDefaultValue

PSR1 (3 sniffs)
---------------
  PSR1.Classes.ClassDeclaration
  PSR1.Files.SideEffects
  PSR1.Methods.CamelCapsMethodName

PSR2 (9 sniffs)
---------------
  PSR2.Classes.ClassDeclaration
  PSR2.Classes.PropertyDeclaration
  PSR2.ControlStructures.ElseIfDeclaration
  PSR2.ControlStructures.SwitchDeclaration
  PSR2.Files.ClosingTag
  PSR2.Files.EndFileNewline
  PSR2.Methods.FunctionCallSignature
  PSR2.Methods.FunctionClosingBrace
  PSR2.Methods.MethodDeclaration

Squiz (15 sniffs)
-----------------
  Squiz.Classes.ValidClassName
  Squiz.ControlStructures.ControlSignature
  Squiz.ControlStructures.ForEachLoopDeclaration
  Squiz.ControlStructures.ForLoopDeclaration
  Squiz.ControlStructures.LowercaseDeclaration
  Squiz.Functions.FunctionDeclaration
  Squiz.Functions.FunctionDeclarationArgumentSpacing
  Squiz.Functions.LowercaseFunctionKeywords
  Squiz.Functions.MultiLineFunctionDeclaration
  Squiz.Scope.MethodScope
  Squiz.WhiteSpace.CastSpacing
  Squiz.WhiteSpace.ControlStructureSpacing
  Squiz.WhiteSpace.ScopeClosingBrace
  Squiz.WhiteSpace.ScopeKeywordSpacing
  Squiz.WhiteSpace.SuperfluousWhitespace
```


## Install
Required composer.

Install PHP_CodeSniffer system-wide: 
````
composer global require "squizlabs/php_codesniffer=*"
````

Copy/clone this repository content to `/global_composer_vendor/squizlabs/php_codesniffer/src/Standards/Dsn12`


## Getting started
`phpcs --standard=Dsn12 /path/to/file-or-directory`

If PHP_CodeSniffer finds any coding standard errors, a report will be shown after running the command.

Full usage information and example reports are available on the https://github.com/squizlabs/PHP_CodeSniffer/wiki/Usage.


List of Installed Coding Standards
`phpcs -i`

Not use an external standard by specifying the full path to the standard's root directory
`phpcs --standard=/path/to/MyStandard /path/to/code/myfile.inc`

Change the default standard
`phpcs --config-set default_standard PSR12`

To view the currently set configuration options
`phpcs --config-show`

To delete a configuration option
`phpcs --config-delete <option>`
